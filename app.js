const express = require("express");
const graphqlHTTP = require("express-graphql");
const schema = require("./schema/schema");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");

app.use(cors());

mongoose.connect("mongodb://localhost/graphQLBook");
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(
  "/graphql",

  graphqlHTTP({
    schema,
    graphiql: true
  })
);

app.listen(4000, () => console.log("listening at 4000"));
